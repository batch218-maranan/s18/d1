// console.log('Hello World');

/*function printInfo(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

printInfo();*/

function printName(firstName){
	console.log("My name is " + firstName)
};
printName("Juana");
printName("John");
printName("Cena");

// Now we have a reusable function / reusable task but could have different output based on what value to process, with the help of...
// [SECTION] Parameters and Arguments

// Parameter - used to store information that is provided to a function when it is called/invoked.
	// firstName acts as named variable/container that exists only inside a function.

// Argument
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called argument.
	// Values passed when invoking a function are called argument.
	// These arguments are then stored as the paramater within the function.

let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can also be passed as an argument

// -------------------------------------

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of "+num+ "divided by 8 is: " + remainder);
		let isDivisiblityBy8 = remainder === 0;
	console.log("Is "+num+ " divisible by 8?")
	console.log(isDivisiblityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECTION] Function as argument
	// Function parameters can also accept functions as arguments.
	// Some complex functions uses other functions to perform more complicated results.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.")
	}

	function invokeFunction(argumentFunction){
		argumentFunction();

	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

// -------------------------------------

// [SECTION] Using Multiple Parameters

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is " + firstName + " " + middleName + " " + lastName)
	}

	createFullName("Mc-Jo", "Nawal", "Maranan");

	// Using variables as an argument
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName)

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference: " + (numA - numB));
	}

	getDifferenceOf8Minus4(8, 4);
	// getDifferenceOf8Minus4(4, 8); - result to logical error

// [SECTION] Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called.

	function returnFullName(firstName, middleName, lastName){

		// return firstName + " " + middleName + " " + lastName;

		// This line of code will not be printed
		console.log("This is printed inside a function");

		let fullName = firstName + " " + middleName + " " + lastName;

		return fullName;
	}

	let completeName = returnFullName("Paul", "Smith", "Jordan")
	console.log(completeName);


	console.log("I am " + completeName);


	function printPlayerInfo(userName, level, job){
		console.log("Username: " + userName);
		console.log("Level: " + level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("boxzMapagmahal", "Senior", "Programmer");
	console.log(user1)